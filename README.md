[Geschäftsordnung der jDPG Regionalgruppe Dortmund](https://gitlab.com/jdpgdortmund/geschaeftsordnung/-/jobs/artifacts/master/raw/build/geschaeftsordnung.pdf?job=release)
===
[![build status](https://gitlab.com/jdpgdortmund/geschaeftsordnung/badges/master/pipeline.svg?job=build&key_text=build&key_width=40)](https://gitlab.com/jdpgdortmund/geschaeftsordnung/-/commits/master)

Dies ist die selbstauferlegte Geschäftsordnung der RG Dortmund. Sie kann durch [hier](https://gitlab.com/jdpgdortmund/geschaeftsordnung/-/jobs/artifacts/master/raw/build/geschaeftsordnung.pdf?job=release)
als PDF gedownloaded werden.

Sie wurde irgendwann von den "Gründern" erstellt und im Dezember 2020 überarbeitet. Ein großteil ist vermutlich von Robert geschrieben. 
