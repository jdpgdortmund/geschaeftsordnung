# TEXTARGET is the output filename TEXTSOURCE is input. Simply change these if you want to compile one .tex file 
TEXTARGET=build/main.pdf
TEXSOURCE=header.tex

all: $(TEXTARGET) $(PYTARGET)

TEXOPTIONS = -lualatex \
		--output-directory=build \
		--interaction=nonstopmode \
		--halt-on-error \
		--use-make \

TEXPREFIX = TEXINPUTS=build: \
	BIBINPUTS=build: \
	max_print_line=1048576 \

clean:
	rm -rf build

build:
	mkdir -p build

# Use this to work on tex-document, it will be updated continuesly by latexmk
work:
	$(TEXPREFIX) latexmk -pvc $(TEXOPTIONS) $(TEXSOURCE)

$(TEXTARGET): $(TEXSOURCE) FORCE | build
	$(TEXPREFIX) latexmk $(TEXOPTIONS) $(TEXSOURCE)

FORCE:

.PHONY: all clean FORCE build
